source "$HOME/.local/share/.cargo/env"
. "$HOME/.local/share/.cargo/env"
# Spring Cleaning
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_STATE_HOME=${XDG_STATE_HOME:="$HOME/.local/state"}

#runtime
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority

#.local/share
export CARGO_HOME="$XDG_DATA_HOME"/.cargo
export GNUPGHOME="$XDG_DATA_HOME"/.gnupg
export GOPATH="$XDG_DATA_HOME"/go
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pasta
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export WINEPREFIX="$XDG_DATA_HOME"/wine

#.config
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc 

#.cache
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history

#.local/state
export HISTFILE="$XDG_STATE_HOME"/zsh/history
